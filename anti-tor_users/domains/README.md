# Kontraŭ-Tor uzantoj FQDN-listo


[//]: # (do not edit me; start)

## *209,442* FQDN

[//]: # (do not edit me; end)


- Ne ĉiuj uzantoj de Tor estas malbonaj. Ne punu ĉiujn.
  - Kiel vi sentas, se iu blokas vin sen kialo?
  - Uzi Tor ne estas krimo.
  - [Fakuloj diras, ke gruppuno estas senutila, kontraŭproduktiva, mallaborema kaj neetika](https://web.archive.org/web/20201112000414/https://mypointexactly.wordpress.com/2009/07/21/group-punishment-ineffective-unethical/).
- Blokado de Tor ne estas solvo. Estas VPNj, retprogramoj kaj prokuroj.


-----

# Anti-Tor users FQDN list

- Not all Tor users are bad. Do not punish everyone.
  - How do you feel if someone block you for no reason?
  - Using Tor is not a crime.
  - Experts say that group punishment is ineffective, counterproductive, lazy and unethical.
- Blocking Tor is not a solution. There are VPNs, network program and proxies.


-----

![](../../image/anonexist.jpg)
