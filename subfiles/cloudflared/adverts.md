## How many % of adverts and trackers are using Cloudflare?


- [Easylist](https://web.archive.org/web/20210516110248/https://easylist.to/)
```
EasyList is the primary filter list that removes most adverts from international webpages, including unwanted frames, images and objects.

EasyPrivacy is an optional supplementary filter list that completely removes all forms of tracking from the internet, including web bugs, tracking scripts and information collectors, thereby protecting your personal data.
```


We picked domain-blocking lines from the list and filtered out domains which has exception rules.
Here's the result.


| Adblock list | Domains Count | Cloudflare | % |
| --- | --- | --- | --- |
| [EasyList](https://easylist.to/easylist/easylist.txt) | 27,595 | 7,516 | 27.2% |
| [EasyPrivacy](https://easylist.to/easylist/easyprivacy.txt) | 16,358 | 4,702 | 28.7% |
| [AdGuard](https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt) | 43,513 | 8,236 | 18.9% |
| [AdAway](https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt) | 2,053 | 657 | 32% |
| Total | 61,102 | 15,053 | 24.6% |


### 24.6% of adverts and trackers are using Cloudflare.