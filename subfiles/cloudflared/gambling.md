## How many % of gambling domains are using Cloudflare?


We downloaded the gambling list from [here](https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/gambling-hosts) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 980 |
| eu | 47 |
| net | 42 |
| ag | 39 |
| uk | 34 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 575 |
| Normal | 816 |


### 41.3% of gambling domains are using Cloudflare.