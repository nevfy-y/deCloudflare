## How many % of VPN services are using Cloudflare?


- [VPN service](https://en.wikipedia.org/wiki/VPN_service)
```
A virtual private network service, commonly known as a VPN service, provides a proxy server to users
who want to avoid Internet censorship circumvention or more specifically geoblocking.
Or users who want to protect their communications against data profiling or MitM attacks on hostile networks. 
```


The following is a list of virtual private network services.


| Name | Site | Cloudflared |
| --- | --- | --- |
| AirVPN | https://airvpn.org/ | No |
| AstrillVPN | https://astrill.com/ | No |
| atlasVPN | https://atlasvpn.com/ | Yes |
| Avast SecureLine VPN | https://www.avast.com/ | Yes |
| Avira | https://www.avira.com/ | Yes |
| AzireVPN | https://www.azirevpn.com/ | No |
| blackVPN | https://blackvpn.com/ | Yes |
| Cloudflare WARP | https://cloudflarewarp.com/ | Yes |
| CrypticVPN | https://crypticvpn.com/ | Yes |
| CyberGhost | https://www.cyberghostvpn.com/ | Yes |
| EarthVPN | https://www.earthvpn.com/ | Yes |
| ExpressVPN | https://www.expressvpn.com/ | No |
| F-Secure Freedome | https://www.f-secure.com/ | Yes |
| FastestVPN | https://fastestvpn.com/ | Yes |
| Ghostpath | https://ghostpath.com/ | No |
| Guardian | https://guardianapp.com/ | No |
| HideMe | https://hide.me/ | Yes |
| HideMyAss | https://www.hidemyass.com/ | Yes |
| Hotspot Shield | https://hotspotshield.com/ | Yes |
| ibVPN | https://www.ibvpn.com/ | No |
| IPredator | https://www.ipredator.se/ | No |
| IPVanish | https://www.ipvanish.com/ | Yes |
| IVPN | https://www.ivpn.net/ | No |
| Le VPN | https://www.le-vpn.com/ | Yes |
| Mullvad | https://mullvad.net/ | No |
| NordVPN | https://nordvpn.com/ | Yes |
| Oeck | https://www.oeck.com/ | No |
| OneVPN | https://www.onevpn.com/ | No |
| Perfect-privacy | https://www.perfect-privacy.com/ | No |
| Private Internet Access | https://www.privateinternetaccess.com/ | Yes |
| ProtonVPN | https://protonvpn.com/ | Yes |
| PureVPN | https://www.purevpn.com/ | Yes |
| SaferVPN | https://safervpn.com/ | Yes |
| Seed4Me | https://seed4.me/ | No |
| Speedify | https://speedify.com/ | Yes |
| StrongVPN | https://www.strongvpn.com/ | Yes |
| SumRando | https://www.sumrando.com/ | No |
| SurfShark | https://surfshark.com/ | Yes |
| SwitchVPN | https://switchvpn.net/ | Yes |
| TigerVPN | https://www.tigervpn.com/ | No |
| TorGuard | https://torguard.net/ | Yes |
| TrustZone | https://trust.zone/ | Yes |
| TunnelBear | https://tunnelbear.com/ | Yes |
| VPNArea | https://vpnarea.com/ | Yes |
| VPNBook | https://www.vpnbook.com/ | No |
| VPNht | https://vpn.ht/ | No |
| VPNSecure | https://www.vpnsecure.me/ | Yes |
| VyprVPN | https://www.vyprvpn.com/ | Yes |
| Windscribe | https://windscribe.com/ | Yes |
| ZenMate | https://zenmate.com/ | Yes |


-----

| Type | Count |
| --- | --- | 
| Cloudflare | 32 |
| Normal | 18 |


### 64% of VPN services are using Cloudflare.
